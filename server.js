/*global require*/

var express = require('express'),
	http = require('http'),
	wkhtmltopdf = require('wkhtmltopdf'),
	formParser = require('./middleware/formParser'),
	app = express(),
	router = express.Router(),
	port = process.env.PORT || 9181,
	limiter = require('./modules/ratelimit')(app),
	logger = require('./modules/log')(app),
	production = (process.env.NODE_ENV || '').toLowerCase() === 'production';

// default route, used by all routes
router.use('/', function (req, res, next) {
	if (!req.headers.host) {
		throw new Error('Header \'host\' required.', 'Invalid Request');
		next();
		return;
	}

	// Disable Cache
	res.set('Cache-Control', 'no-store, no-cache');
	res.set('Pragma', 'no-cache');
	res.set('Expires', '0');

	// Cross-Origin Resource Sharing
	res.set('Access-Control-Allow-Origin', req.headers.host);

	// Allowed Methods
	res.set('Access-Control-Allow-Methods', 'GET,POST');

	// Disable browser content-type sniffing
	res.set('X-Content-Type-Options', 'nosniff');

	// Enable browser XSS protection in IE9+
	res.set('X-XSS-Protection', '1; mode=block');

	// Only allow frames from same origin
	res.set('X-Frame-Options', 'SAMEORIGIN');

	// Allowed Headers
	//res.set('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type');

	// Remove Server Headers
	res.removeHeader('X-Powered-By');
	res.removeHeader('server');
	next();
});

// ping
router.use('/ping', function (req, res) {
	res.status(200).json({
		ok: true
	});
});

// print
router.use('/print', formParser, function (req, res, next) {
	var args = req.body || req.query,
		html = args.html,
		filename = args.filename;

	// don't send html with args to wkhtmltopdf
	delete args.html;
	delete args.filename;

	// debug
	args.debug = !production;

	// defaults
	if (!args.footerLeft)
		args.footerLeft = '[date]';
	if (!args.footerRight)
		args.footerRight = 'Page [page] of [topage]';
	if (!args.marginBottom)
		args.marginBottom = '7mm';
	if (!args.marginTop)
		args.marginTop = '7mm';
	if (!args.marginLeft)
		args.marginLeft = '7mm';
	if (!args.marginRight)
		args.marginRight = '7mm';

	if (!html)
		next(new Error('Parameter \'html\' required.'));
	else {
		try {
			res.header('Content-Type', 'application/pdf');
			if (filename)
				res.header('Content-Disposition', 'attachment;filename=' + filename);
			wkhtmltopdf(html, args).pipe(res);
		} catch (e) {
			next(e);
		}
	}
});

// test
router.use('/test', formParser, function (req, res, next) {
	res.header('Content-Type', 'application/pdf');
	wkhtmltopdf('http://google.com/', {
		pageSize: 'Letter',
		marginBottom: '7mm',
		marginTop: '7mm',
		marginLeft: '7mm',
		marginRight: '7mm',
		footerLeft: '[date]',
		footerRight: 'Page [page] of [topage]'
	}).pipe(res);
});

// public directory
app.use(express.static('public'));

// 404
router.use(function (req, res, next) {
	res.header('Content-Type', 'text/plain').json({
		success: false,
		code: 404,
		error: 'Not found'
	});
});

// 500
router.use(function (error, req, res, next) {
	var result = {
		success: false,
		code: 500,
		error: error.message || 'Internal Server Error'
	};
	if (app.get('env') === 'development')
		result.stack = error.stack;
	res.header('Content-Type', 'text/plain').json(result);
});

// server
http.Server(app);

// app router
app.use(router);

if (port > 0) {
	// I like to move it, move it...
	app.listen(port, function () {
		console.log('Starting HTML to PDF Server on port: ', port);
	});
} else
	throw Error('Unable to bind application to port. No port specified.');