var winston = require('winston'),
	morgan = require('morgan'),
	logger = new winston.Logger(),
	util = require('util');

// create stream for morgan logs
logger.stream = {
	write: function(message, encoding){
		logger.info(message);
	}
};

module.exports = function(app) {
	// Log consoles to application.log
	switch((process.env.NODE_ENV || '').toLowerCase()) {
		case 'production':
			logger.add(winston.transports.File, {
				filename: __dirname + '/application.log',
				handleExceptions: true,
				exitOnError: false,
				level: 'warn'
			});
			break;
		case 'test':
			// Don't set up the logger overrides
			return;
		default:
			logger.add(winston.transports.Console, {
				colorize: true,
				timestamp: true,
				level: 'info'
			});
			break;
	}

	// request logging
	app.use(morgan('{"remote_addr": ":remote-addr", "remote_user": ":remote-user", "date": ":date[clf]", "method": ":method", "url": ":url", "http_version": ":http-version", "status": ":status", "result_length": ":res[content-length]", "referrer": ":referrer", "user_agent": ":user-agent", "response_time": ":response-time"}', {
		stream: logger.stream
	}));

	// format arguments passed to console functions
	function formatArgs(args){
		return [util.format.apply(util.format, Array.prototype.slice.call(args))];
	}

	// override console functions with winston functions
	console.log = function(){
		logger.info.apply(logger, formatArgs(arguments));
	};
	console.info = function(){
		logger.info.apply(logger, formatArgs(arguments));
	};
	console.warn = function(){
		logger.warn.apply(logger, formatArgs(arguments));
	};
	console.error = function(){
		logger.error.apply(logger, formatArgs(arguments));
	};
	console.debug = function(){
		logger.debug.apply(logger, formatArgs(arguments));
	};

	return logger;
};