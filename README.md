HTML To PDF Server
==================

Small Node.js application providing an API for wkhtmltopdf.

## Install

1. Install [Node.js](http://nodejs.org/) >= 4.2.6
1. Install [wkhtmltopdf](http://wkhtmltopdf.org/downloads.html) >= 0.12.2
1. Open a terminal and clone the repository by running `git clone --recursive https://bitbucket.org/geo-jobe/htmltopdf.git`. Change directory to the project's folder.
1. Install the project's Node.js dependencies by running `npm install`. If you have errors, make sure terminal is in administrative mode.
1. Run the web server with `npm start`

## Server Deployment

The Node.js application runs on port 9181 by default. In order to access the application from the Internet without Firewall issues, a reverse proxy or load-balancer must be used.

### IIS Server Requirements
1. IIS - [website](https://www.iis.net/)
1. URL Rewrite Module - [download](https://www.iis.net/downloads/microsoft/url-rewrite)
1. Application Request Routing Module - [download](https://www.iis.net/downloads/microsoft/application-request-routing)

### NGINX Server Requirements
1. NGINX - [website](https://www.nginx.com/)

## Structure
```
.
├── middleware                      # Application middlewares
│   └── formParser.js               # Middleware for parsing AJAX and forms using Busboy
├── modules                         # Application modules
│   ├── log.js                      # Overrides default console commands with Winston hooks
│   └── ratelimit.js                # Implements rate-limiting for Express
├── README.md                       # You are HERE
├── server.js                       # Application server
└── web.config                      # IIS reverse-proxy configuration
```

## Issues

Find a bug or want to request a new feature?  Please let us know by submitting an issue to our [Issue Tracker](https://bitbucket.org/geo-jobe/htmltopdf/issues?status=new&status=open)

## Licensing
Copyright 2016 GEO Jobe GIS Consulting.