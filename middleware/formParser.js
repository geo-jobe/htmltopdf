var Busboy = require('busboy');

module.exports = function (req, res, next) {
	var contentType = req.headers['content-type'] && req.headers['content-type'].toLowerCase();
	// only process forms & ajax requests
	if (['multipart/form-data', 'application/x-www-form-urlencoded'].indexOf(contentType) === -1)
		next();
	else {
		var busboy = new Busboy({
			immediate: true,
			headers: req.headers
		});

		busboy.on('field', function (field, val) {
			if (!req.body)
				req.body = {};
			req.body[field] = val;
		});

		busboy.on('error', function (err) {
			throw err;
		});

		busboy.on('finish', function () {
			next();
		});

		req.pipe(busboy);
	}
};